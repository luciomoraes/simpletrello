﻿using System;
using System.Collections.Generic;
using System.Web.Configuration;
using System.Web.Mvc;
using TrelloClient.Models;
using TrelloNet;


namespace TrelloClient.Controllers
{

    public class AccountController : Controller
    {
        private ITrello trello = new Trello(WebConfigurationManager.AppSettings["TrelloAppKey"]);

        public AccountController()
        {
        }
        public ActionResult Index()
        {
            return View();
        }
     
        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model)
        {
            model.TrelloAuthenticationRequestUri = trello.GetAuthorizationUrl("Simple Trello", Scope.ReadWrite);
            
            if (ModelState.IsValid)
            {
                return View("ExternalLoginConfirmation", model);
            }
            return View();
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLoginConfirmation(LoginViewModel model)
        {
            try
            {
                Session["token"] = model.Token;
                Session["trello"] = trello;
                trello.Authorize(model.Token);

                Member me = trello.Members.Me();
                IEnumerable<Card> cards = trello.Cards.ForMember(me);
                IEnumerable<Board> boards = trello.Boards.ForMember(me);
                
                ViewBag.cards = cards;
                ViewBag.me = me;

                Session["me"] = me;
                Session["cards"] = cards;
                Session["boards"] = boards;

                List<SimpleCardModel> listCard = new List<SimpleCardModel>(); 
                foreach (var card in cards)
                {
                    SimpleCardModel t = new SimpleCardModel();
                    
                    t.Id = card.Id;
                    t.Name = card.Name;
                    t.BoardId = card.IdBoard;
                    listCard.Add(t);
                }

                List<SimpleBoardModel> listBoard = new List<SimpleBoardModel>();
                foreach (var board in boards)
                {
                    SimpleBoardModel t = new SimpleBoardModel();

                    t.Id = board.Id;
                    t.Name = board.Name;
                    listBoard.Add(t);
                }


                Session["simplecardmodel"] = listCard;
                Session["simpleboardmodel"] = listBoard;

                if (me.Username != model.User)
                {
                    return View("ExternalLoginFailure");
                }
                if (ModelState.IsValid)
                {
                    return RedirectToAction("Index","Boards");
                }
            } catch (ArgumentException e)
            {
                Console.WriteLine("{O} Exception caught", e);
            }
            return View();
        }
        
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }
    }
}
