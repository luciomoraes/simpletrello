﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrelloNet;

namespace TrelloClient.Models
{
    public class CardsController : Controller
    {
        // GET: Cards
        [HttpGet]
        public ViewResult Index(string Id)
        {
            List<SimpleCardModel> simpleCardModel = (List<SimpleCardModel>)Session["simplecardmodel"];
            var boardid = Session["id"];
            ViewBag.boardidfrom = boardid;
            return View(simpleCardModel);
        }

        [HttpGet]
        [AllowAnonymous]
        public ViewResult Comment(string Id)
        {
            var commentModel = new CommentModel
            {
                CardId = Id
            };
            ViewBag.Cardid = commentModel.CardId;
            return View(commentModel);
        }


        [HttpPost]
        [AllowAnonymous]
        public ActionResult AddComment(CommentModel comment)
        {

            ITrello trello = (Trello)Session["trello"];
            Member me = (Member)Session["me"];
            IEnumerable<Card> cards = (IEnumerable<Card>)Session["cards"];
            string token = Convert.ToString(Session["token"]);
            trello.Authorize(token);

            if (comment != null && ModelState.IsValid) {
                foreach (var card in cards)
                {
                    if (card.Id == comment.CardId)
                    {
                        trello.Cards.AddComment(card, comment.Text);
                    }
                }
                return RedirectToAction("Index", "Cards");
            }
            return View();
        }

        [AllowAnonymous]
        public ActionResult CommentFailure()
        {
            return View("CommentFailure");
        }
    }
}