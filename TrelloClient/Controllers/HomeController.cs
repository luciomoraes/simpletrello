﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrelloNet;

namespace TrelloClient.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        
        public ActionResult Index()
        {
            Member me = (Member)Session["me"];
            string token = Convert.ToString(Session["token"]);
            if (me != null && token != null)
            {
                return RedirectToAction("Index", "Boards");
            }
            return RedirectToAction("Login", "Account");
        }
    }
}