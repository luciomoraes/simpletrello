﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrelloClient.Models;
using TrelloNet;

namespace TrelloClient.Controllers
{
    public class BoardsController : Controller
    {
        // GET: Boards
        [HttpGet]
            public ViewResult Index()
        {
            List<SimpleBoardModel> simpleBoardModel = (List<SimpleBoardModel>)Session["simpleboardmodel"];
            return View(simpleBoardModel);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult GetCards(string Id)
        {
            var simpleBoardModel = new SimpleBoardModel
            {
                Id = Id
            };
            Session["id"] = simpleBoardModel.Id;

            return RedirectToAction("Index","Cards");
        }
    }
}