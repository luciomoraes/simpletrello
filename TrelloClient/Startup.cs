﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TrelloClient.Startup))]
namespace TrelloClient
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
