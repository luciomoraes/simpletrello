﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrelloClient.Models
{
    public class LoginViewModel
    {
        [Required]
        [Display(Name = "User")]
        public string User { get; set; }

        [Display(Name = "Key")]
        public string Key { get; set; }

        public Uri TrelloAuthenticationRequestUri { get; set; }

        [Display(Name = "Token")]
        public string Token { get; set; }
    }
}
