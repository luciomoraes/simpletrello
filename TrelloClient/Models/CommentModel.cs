﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrelloClient.Models
{
    public class CommentModel
    {
        public string CardId { get; set; }
        
        public string Text { get; set; }
    }
}