﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrelloClient.Models
{
    public class SimpleBoardModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}