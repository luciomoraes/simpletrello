﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrelloClient.Models
{
    public class ViewSimpleCardModels
    {
        public IEnumerable<SimpleCardModel> SimpleCards { get; set; }
    }
}