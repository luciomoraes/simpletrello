﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrelloClient.Util
{
    public sealed class TrelloSingleton
    {
        private static TrelloSingleton instance = new TrelloSingleton();
        public string TrelloAppKey { get; set; }

        private TrelloSingleton() { }

        public static TrelloSingleton Instance
        {
            get
            {
                if (instance == null)
                    instance = new TrelloSingleton();
                return instance;
            }
        }
    }
}